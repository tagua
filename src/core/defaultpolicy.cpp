/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "defaultpolicy.h"
#include "board.h"
#include "piece.h"
#include "state.h"

InteractionType DefaultPolicy::movable(const IState* state, 
                                       const TurnTest& test, 
                                       const Point& p) const {
  Piece piece = state->board()->get(p);
  if (piece == Piece() || !test(piece.color()))
    return NoAction;
    
  return piece.color() == state->turn() ? Moving : Premoving;
}

InteractionType DefaultPolicy::droppable(const IState* state,
                                       const TurnTest& test, 
                                       const IColor* pool) const {
  if (!test(pool))
    return NoAction;

  return pool == state->turn() ? Moving : Premoving;
}
