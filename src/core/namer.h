/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__NAMER_H
#define CORE__NAMER_H

#include <QString>
#include "export.h"

class Piece;

/**
  * @brief A simple component used to retrieve symbolic piece names.
  */
class TAGUA_EXPORT INamer {
public:
  virtual ~INamer();
    
  virtual QString name(const Piece& piece) const = 0;
};

#endif // CORE__NAMER_H

