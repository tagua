/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__ANIMATION_H
#define CORE__ANIMATION_H

#include <boost/enable_shared_from_this.hpp>
#include "animation_fwd.h"
#include "export.h"

class TAGUA_EXPORT Animation : public boost::enable_shared_from_this<Animation> {
protected:
  enum State {
    Active,
    Inactive,
    Aborted
  };
public:
  virtual ~Animation();
  virtual bool sticky() const;

  virtual State animationAdvance(int msec) = 0;
  virtual void stop() = 0;
  virtual void abort() = 0;
  
  virtual void addPreAnimation(AnimationPtr anim);
  virtual void addPostAnimation(AnimationPtr anim);
};

#endif // CORE__ANIMATION_H

