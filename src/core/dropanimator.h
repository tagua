/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__DROPANIMATOR_H
#define CORE__DROPANIMATOR_H

#include "baseanimator.h"
#include "component.h"
#include "export.h"

class IColor;
class INamer;
class TaguaAPI;

class TAGUA_EXPORT DropAnimator : public BaseAnimator {
Q_OBJECT
  IWarper* m_warper;
  IAnimator* m_base;
  const IColor** m_players;
public:
  virtual ~DropAnimator();
  
  DropAnimator(IAnimator* base, TaguaAPI* api, 
               const INamer* namer, const IColor** players);

  virtual AnimationPtr warp(const IState* state);
  virtual AnimationPtr forward(const Move& move, const IState* state);
  virtual AnimationPtr back(const Move& move, const IState* state);
  virtual void setWarper(IWarper* warper);
protected Q_SLOTS:  
  virtual void updatePool(const IState* final);
};

#endif // CORE__DROPANIMATOR_H
