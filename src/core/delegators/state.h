/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef DELEGATORS__STATE_H
#define DELEGATORS__STATE_H

#include "../state.h"

/**
  * @brief Namespace holding delegators to core Tagua components.
  */
namespace Delegators {

class State : public IState {
protected:
  IState* m_state;
public:
  State(IState* state) : m_state(state) { m_state->setDelegator(this); }
  virtual ~State() { delete m_state; }
  
  virtual IState* clone() const { return new State(m_state->clone()); }
  virtual void setup() { m_state->setup(); }
  virtual const Board* board() const { return m_state->board(); }
  virtual Board* board() { return m_state->board(); }
  virtual const IColor* turn() const { return m_state->turn(); }
  virtual void setTurn(const IColor* turn) { m_state->setTurn(turn); }
  virtual bool equals(IState* other) const { return m_state->equals(other); }
  virtual void assign(const IState* other) { m_state->assign(other); }
  virtual void move(const Move& move) { m_state->move(move); }
  virtual TaguaObject* flags() { return m_state->flags(); }
  virtual const TaguaObject* flags() const { return m_state->flags(); }
  virtual int rank(int n, const IColor* turn) const { return m_state->rank(n, turn); }
  virtual const IPoolCollection* pools() const { return m_state->pools(); }
  virtual IPoolCollection* pools() { return m_state->pools(); }
  virtual const IBehaviour* behaviour() const { return m_state->behaviour(); }
  virtual void setDelegator(IState* state) { m_state->setDelegator(state); }
};

}

#endif // DELEGATORS__STATE_H
