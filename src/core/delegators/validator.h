/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef DELEGATORS_VALIDATOR_H
#define DELEGATORS_VALIDATOR_H

#include "../validator.h"

namespace Delegators {

class Validator : public IValidator {
protected:
  IValidator* m_validator;
public:
  Validator(IValidator* validator) 
  : m_validator(validator) { 
    m_validator->setDelegator(this); 
  }
  virtual ~Validator() { delete m_validator; }
  virtual bool pseudolegal(const IState* state, Move& move) const {
    return m_validator->pseudolegal(state, move);
  }
  virtual bool legal(const IState* state, Move& move) const {
    return m_validator->legal(state, move);
  }
  virtual bool attacks(const IState* state, const IColor* player, 
                        const Point& square, const Piece& target = Piece()) const {
    return m_validator->attacks(state, player, square, target);
  }
  virtual const IColor* mover(const IState* state, const Move& move) const {
    return m_validator->mover(state, move);
  }
  virtual void setDelegator(IValidator* delegator) {
    return m_validator->setDelegator(delegator);
  }
};

}

#endif // DELEGATORS_VALIDATOR_H
