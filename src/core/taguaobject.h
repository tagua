/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__TAGUAOBJECT_H
#define CORE__TAGUAOBJECT_H

#include <QMap>
#include <QVariant>
#include "export.h"

class TAGUA_EXPORT TaguaObject {
public:
  typedef QMap<QString, QVariant> Properties;
protected:
  Properties m_prop;
public:
  QVariant get(const QString& key) const;
  void set(const QString& key, const QVariant& value);
  
  bool operator==(const TaguaObject& other) const;
  bool operator!=(const TaguaObject& other) const;
};

#endif // CORE__TAGUAOBJECT_H
