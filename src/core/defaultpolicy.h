/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__DEFAULTPOLICY_H
#define CORE__DEFAULTPOLICY_H

#include "component.h"
#include "policy.h"

class TAGUA_EXPORT DefaultPolicy : public Component, public IPolicy {
Q_OBJECT
public Q_SLOTS:
  virtual InteractionType movable(const IState* state,
                                  const TurnTest& test, 
                                  const Point& p) const;

  virtual InteractionType droppable(const IState* state,
                                  const TurnTest& test,
                                  const IColor* pool) const;
};

#endif // CORE__DEFAULTPOLICY_H

