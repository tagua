/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__TAGUA_CAST_H
#define CORE__TAGUA_CAST_H

#include <KDebug>

#define CAT_AUX(x, y) x ## y
#define CAT(x, y) CAT_AUX(x, y)

#ifdef TAGUA_DEBUG
  #define TAGUA_CAST(ID, TYPE, RETVAL) \
    TYPE* ID = dynamic_cast<TYPE*>(CAT(ID, _)); \
    if (!ID) { \
      kWarning("Mismatch!!"); \
      return RETVAL; \
    }
#else
  #define TAGUA_CAST(ID, TYPE, RETVAL) \
    TYPE* ID = static_cast<TYPE*>(CAT(ID, _));
#endif

#endif // CORE__TAGUA_CAST_H
