/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__STATE_H
#define CORE__STATE_H

#include "taguaobject.h"

class Board;
class IBehaviour;
class IColor;
class IPoolCollection;
class Move;

/**
  * @brief The state of the game in a given moment of time.
  * 
  * A State component encapsulates the global state of the game, i.e.
  * what pieces are on the board or on the pool, and game flags like
  * the en-passant square for chess.
  * 
  * A State component also provides methods to setup the initial
  * configuration and to make a move.
  */
class TAGUA_EXPORT IState {
public:
  virtual ~IState();
  
  virtual IState* clone() const = 0;
  
  /**
    * Setup the initial configuration of pieces and reset all flags.
    */
  virtual void setup() = 0;
  
  /**
    * @return The piece board.
    */
  virtual const Board* board() const = 0;
  virtual Board* board() = 0;
  
  /**
    * @return The player who plays next.
    */
  virtual const IColor* turn() const = 0;
  
  /**
    * Change the current turn.
    */
  virtual void setTurn(const IColor* turn) = 0;
  
  /**
    * @return Whether two states are equal.
    */
  virtual bool equals(IState* other) const = 0;
  
  /**
    * Make this state equal to the given one.
    */
  virtual void assign(const IState* other) = 0;

  /**
    * Play a move.
    */
  virtual void move(const Move& move) = 0;
  
  /**
    * @return Current flags.
    */
  virtual TaguaObject* flags() = 0;
  virtual const TaguaObject* flags() const = 0;
  
  /**
    * @return The absolute index of the n-th rank for a given player.
    */
  virtual int rank(int n, const IColor* turn) const = 0;
  
  /**
    * @return The pool collection associated to this state.
    */
  virtual const IPoolCollection* pools() const = 0;
  virtual IPoolCollection* pools() = 0;
  
  /**
    * @return The behaviour object associated to this state.
    */
  virtual const IBehaviour* behaviour() const = 0;
  
  virtual void setDelegator(IState* state) = 0;
};

#endif // CORE__STATE_H
