/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__POOL_H
#define CORE__POOL_H

class Piece;

/**
  * @brief A collection of pieces.
  * 
  * A pool is a collection of pieces, typically displayed on the side
  * of the board. Some games define special rules for moving pieces
  * into and out from the pools.
  */
class IPool {
public:
  virtual ~IPool();
  
  virtual IPool* clone() const = 0;
  
  /**
    * @return Whether two pools are equal, i.e. contain the same pieces.
    */
  virtual bool equals(const IPool* other) const = 0;
  
  /**
    * @return Whether this pool is empty.
    */
  virtual bool empty() const = 0;
  
  /**
    * @return The number of pieces in this pool.
    */
  virtual int size() const = 0;
  
  /**
    * Insert a new piece into the pool.
    * Note that @a index is just a hint where the piece is to be
    * added, and pool implementations may decide to put the piece
    * at a different index.
    * @returns The actual index where the newly inserted piece can be found.
    */
  virtual int insert(int index, const Piece& piece) = 0;
  
  /**
    * Retrieve a piece from the pool.
    * @param index The index of the piece to retrieve.
    */
  virtual Piece get(int index) const = 0;
  
  /**
    * Delete a piece from the pool.
    * @param index The index of the piece to delete.
    * @returns The piece removed from the pool by this call.
    */
  virtual Piece take(int index) = 0;
  
  /**
    * Delete a piece from the pool.
    * @param piece The piece to delete.
    * @returns Whether the specified piece has successfully been deleted.
    */
  virtual bool take(const Piece& piece) = 0;
};

#endif // CORE__POOL_H
