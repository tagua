/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "defaultpool.h"
#include "piece.h"
#include "taguacast.h"
#include "type.h"

DefaultPool::Key::Key(const IType* type) : type(type) { }

bool DefaultPool::Key::operator<(const Key& other) const {
  return type->index() < other.type->index();
}

DefaultPool::DefaultPool(const IColor* owner)
: m_owner(owner) { }

DefaultPool::~DefaultPool() { }

IPool* DefaultPool::clone() const {
  DefaultPool* c = new DefaultPool(m_owner);
  c->m_data = m_data;
  return c;
}

bool DefaultPool::equals(const IPool* other_) const {
  TAGUA_CAST(other, const DefaultPool, false);
  return other->m_data == m_data;
}

bool DefaultPool::empty() const {
  for (Data::const_iterator it = m_data.begin(),
       end = m_data.end(); it != end; ++it) {
    if (it.value() > 0) return false;
  }
  
  return true;
}

int DefaultPool::size() const {
  int res = 0;
  for (Data::const_iterator it = m_data.begin(),
       end = m_data.end(); it != end; ++it) {
    res += it.value();
  }
  return res;
}

int DefaultPool::insert(int index, const Piece& piece) {
  int fill = 0;
  for (Data::const_iterator it = m_data.begin(),
       end = m_data.end(); 
       it != end && it.key() < Key(piece.type());
       ++it) {
    fill += it.value();
  }

  int nump = add(piece.type());

  if (index < fill)
    return fill;
  if (index >= fill + nump)
    return fill + nump - 1;
  return index;
}

Piece DefaultPool::get(int index) const {
  if (index < 0)
    return Piece();

  int fill = 0;
  for (Data::const_iterator it = m_data.begin(),
       end = m_data.end(); it != end; ++it) {
    if (index < fill + it.value())
      return Piece(m_owner, it.key().type);
    fill += it.value();
  }
  
  return Piece();
}

Piece DefaultPool::take(int index) {
  if (index < 0)
    return Piece();

  int fill = 0;
  for (Data::const_iterator it = m_data.begin(),
       end = m_data.end(); it != end; ++it) {
    if (index < fill + it.value()) {
      const IType* type = it.key().type;
      remove(type);
      return Piece(m_owner, type);
    }
    
    fill += it.value();
  }
  
  return Piece();
}

bool DefaultPool::take(const Piece& piece) {
  if (m_data[Key(piece.type())] <= 0) return false;
  remove(piece.type());
  return true;
}

const IColor* DefaultPool::color() const {
  return m_owner;
}

int DefaultPool::add(const IType* type) {
  return ++m_data[Key(type)];
}

int DefaultPool::remove(const IType* type) {
  Key key(type);
  int n = --m_data[key];
  if (n <= 0) {
    m_data.remove(key);
    return 0;
  }
  
  return n;
}

int DefaultPool::count(const IType* type) const {
  return m_data.value(Key(type));
}
