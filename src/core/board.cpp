/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "board.h"
#include "piece.h"

#include <KDebug>

Board::Board(const Point& size)
: m_size(size) {
  m_data.resize(m_size.x * m_size.y);
}

Board::Board(const Board* other)
: m_size(other->m_size)
, m_data(other->m_data) { }

bool Board::equals(const Board* other) const {
  if (m_size != other->m_size)
    return false;
    
  const unsigned int total = m_data.size();
  for (unsigned int i = 0; i < total; i++) {
    if (m_data[i] != other->m_data[i])
      return false;
  }
  
  return true;
}

Point Board::size() const { return m_size; }

Piece Board::get(const Point& p) const {
  if (valid(p)) {
    return m_data[p.x + p.y * m_size.x];
  }
  else {
    return Piece();
  }
}

void Board::set(const Point& p, const Piece& piece) {
  if (valid(p)) {
    m_data[p.x + p.y * m_size.x] = piece;
  }
  else
    kError() << "point is not valid" << p;
}

bool Board::valid(const Point& p) const {
  return p.x < m_size.x &&
         p.x >= 0       &&
         p.y < m_size.y &&
         p.y >= 0;
}

PathInfo Board::path(const Point& from, const Point& to) const {
  if (!valid(from) || !valid(to))
    return PathInfo(PathInfo::Undefined, 0);
    
  Point delta = to - from;
  PathInfo::Direction direction;

  if (delta.x == 0)
    direction = PathInfo::Vertical;
  else if (delta.y == 0)
    direction = PathInfo::Horizontal;
  else if (delta.x == delta.y)
    direction = PathInfo::Diagonal1;
  else if (delta.x == -delta.y)
    direction = PathInfo::Diagonal2;
  else
    direction = PathInfo::Undefined;

  int num_obs = 0;
  if (direction != PathInfo::Undefined) {
    Point step = delta.normalizeInfinity();
    Point position = from;
    while ((position += step) != to) {
      if (get(position) == Piece())
        continue;
      num_obs++;
    }
  }

  return PathInfo(direction, num_obs);
}

Point Board::find(const Piece& piece) const {
  for (int i = 0; i < m_size.x; i++) {
    for (int j = 0; j < m_size.y; j++) {
      Point p(i, j);
      if (get(p) == piece)
        return p;
    }
  }
  
  return Point::invalid();
}

QStringList Board::borderCoords() const {
  QStringList retv;
  Point p = size();
  for (int i = 0; i < p.x; i++)
    retv << Point(i, 0).col();
  for (int i = 1; i <= p.y; i++)
    retv << QString::number(i);
  return retv + retv;
}

