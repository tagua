/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__BEHAVIOUR_H
#define CORE__BEHAVIOUR_H

#include "export.h"
#include "point.h"

class IColor;
class IState;
class Move;

/**
  * @brief Encapsulates general rules on piece movement.
  */
class TAGUA_EXPORT IBehaviour {
public:
  virtual ~IBehaviour();
    
  /**
    * Perform a capture on a given square. In variants with drops, this
    * adds the captured piece to the opponent pool. In games like chess,
    * this does nothing.
    */
  virtual void captureOn(IState* state, const Point& square) const = 0;
  
  /**
    * Set the moving piece in its destination square, without affecting
    * the current turn or flags.
    * This is a helper function normally called by move.
    */
  virtual void move(IState* state, const Move& m) const = 0;
    
  /**
    * Set next turn as the current turn.
    */
  virtual void advanceTurn(IState* state) const = 0;
  
  /**
    * @return The square where a given move would capture if executed.
    */
  virtual Point captureSquare(const IState* state, const Move& m) const = 0;
      
  /**
    * @return The opponent of @a color.
    */
  virtual const IColor* opponent(const IColor* player) const = 0;
  
  /**
    * Players can have associated directions. For example, in chess,
    * white pieces move towards the 8th rank.
    * Associated directions are often used during move validation.
    * @return Direction associated with @a player.
    */
  virtual Point direction(const IColor* player) const = 0;
};

#endif // CORE__BEHAVIOUR_H
