/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "taguaobject.h"

QVariant TaguaObject::get(const QString& key) const {
  return m_prop[key];
}

void TaguaObject::set(const QString& key, const QVariant& value) {
  m_prop[key] = value;
}

bool TaguaObject::operator==(const TaguaObject& other) const {
  return m_prop == other.m_prop;
}

bool TaguaObject::operator!=(const TaguaObject& other) const {
  return !(operator==(other));
}

