/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "components.h"

#include <KDebug>

#include <core/component.h>
#include <core/animator.h>
#include <core/color.h>
#include <core/moveserializer.h>
#include <core/namer.h>
#include <core/policy.h>
#include <core/state.h>
#include <core/validator.h>

Components::Components(Variant* variant) {
  setVariant(variant);
}

void Components::setVariant(Variant* variant) {
  if (!variant)
    kError() << "Creating a component collection using a null variant";
    
  m_variant = std::auto_ptr<Variant>(variant);
  
  m_animator_factory = requestComponent<IAnimatorFactory>(
    m_variant.get(), "animator_factory");
  m_state = requestComponent<IState>(
    m_variant.get(), "state");
    
  Repository::ComponentMap serializers = 
    m_variant->repository()->listComponents("move_serializer/");
  for (Repository::ComponentMap::iterator it = serializers.begin(),
       end = serializers.end(); it != end; ++it) {
    IMoveSerializer* s = requestInterface<IMoveSerializer>(it.value());
    if (s) m_move_serializers[it.key()] = s;
  }
  
  m_namer = requestComponent<INamer>(m_variant.get(), "namer");
  m_policy = requestComponent<IPolicy>(m_variant.get(), "policy");
  m_validator = requestComponent<IValidator>(m_variant.get(), "validator");
  
  Repository::ComponentMap players =
    m_variant->repository()->listComponents("player");
  for (Repository::ComponentMap::iterator it = players.begin(),
      end = players.end(); it != end; ++it) {
    bool ok;
    int index = it.key().toInt(&ok);
    if (!ok) continue;
    IColor* color = requestInterface<IColor>(it.value());
    if (!color) continue;
    
    m_players[index] = color;
  }
}

IAnimator* Components::createAnimator(TaguaAPI* api) const {
  return m_animator_factory->create(api, m_namer);
}

IState* Components::createState() const {
  return m_state->clone();
}

IMoveSerializer* Components::moveSerializer(const QString& type) {
  return m_move_serializers[type];
}

INamer* Components::namer() { return m_namer; }

IPolicy* Components::policy() { return m_policy; }

IValidator* Components::validator() { return m_validator; }

IColor* Components::player(int index) { return m_players[index]; }

QMap<int, IColor*> Components::players() { return m_players; }

Variant* Components::variant() { return m_variant.get(); }
