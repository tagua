/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__VARIANT_H
#define CORE__VARIANT_H

#include <core/repository.h>

/**
  * @brief A Variant instance represents a game supported in Tagua.
  */
class TAGUA_EXPORT Variant {
  QString m_name;
  QString m_proxy;
  bool m_hidden;
  Repository* m_repo;
public:
  Variant(const QString& name, Repository* repo,
          const QString& proxy = QString(), 
          bool hidden = false);
  
  ~Variant();
  
  QString name() const;
  
  QString proxy() const;
  
  bool hidden() const;
  
  const Repository* repository() const;
  Repository* repository();
};

template <typename Interface>
Interface* requestComponent(Variant* var, const QString& component) {
  return requestInterface<Interface>(var->repository()->getComponent(component));
}

#endif // CORE__VARIANT_H
