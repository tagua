/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CRAZYHOUSE__VALIDATOR_H
#define CRAZYHOUSE__VALIDATOR_H

#include <core/component.h>
#include <core/delegators/validator.h>

class IState;
class Move;

namespace Crazyhouse {

class Validator : public Component, public Delegators::Validator {
Q_OBJECT
public:
  Validator(IValidator* validator);
  virtual ~Validator();
  
  virtual bool pseudolegal(const IState* state, Move& move) const;
  const IColor* mover(const IState* state, const Move& move) const;
};

} // namespace Crazyhouse

#endif // CRAZYHOUSE__VALIDATOR_H

