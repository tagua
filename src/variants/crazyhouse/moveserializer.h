/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CRAZYHOUSE__MOVESERIALIZER_H
#define CRAZYHOUSE__MOVESERIALIZER_H

#include <core/component.h>
#include <core/delegators/moveserializer.h>

namespace Crazyhouse {

class MoveSerializer : public Component, public Delegators::MoveSerializer {
public:
  MoveSerializer(IMoveSerializer* serializer);
  
  virtual QString serialize(const Move& move, const IState* ref) const;
//   virtual Move deserialize(const QString& str, const IState* ref) const;
};

} // namespace Crazyhouse

#endif // CRAZYHOUSE__MOVESERIALIZER_H

