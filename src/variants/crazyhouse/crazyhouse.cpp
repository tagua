/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include <core/board.h>
#include <core/color.h>
#include <core/repository.h>
#include <core/type.h>
#include <core/variantloader.h>

#include "animator.h"
#include "behaviour.h"
#include "moveserializer.h"
#include "state.h"
#include "validator.h"

#include <KDebug>

using namespace Crazyhouse;

#define LOAD_TYPE(NAME) \
  NAME = requestInterface<IType>(chess->getComponent("type/" #NAME))

namespace Crazyhouse {
  const IType* pawn;
  const IType* king;
  const IType* queen;
  const IType* bishop;
  const IType* rook;
  const IType* knight;
  const IColor* players[2];
}

extern "C" KDE_EXPORT Repository* 
taguacrazyhouse_initrepo(IVariantLoader* loader) {
  Repository* repo = new Repository;
  Repository* chess = loader->getRepository("chess");
  if (!chess) return 0;
  repo->setProxy(chess);
  
  // get chess state factory
  Component* chess_state_comp = chess->getComponent("state");
  IState* chess_state = requestInterface<IState>(chess_state_comp);
    
  // create crazyhouse behaviour
  const IBehaviour* behaviour = new Behaviour(chess_state->behaviour());
  
  // create crazyhouse state factory
  Component* state_component = 0;
  bool ok = QMetaObject::invokeMethod(chess_state_comp, "clone",
    Q_RETURN_ARG(Component*, state_component),
    Q_ARG(const IBehaviour*, behaviour),
    Q_ARG(Component*, chess->getComponent("extra/castling_rules")),
    Q_ARG(Point, chess_state->board()->size()));
  kDebug() << "cloning prototype state" << ok;
  State* state = new State(requestInterface<IState>(state_component));
    
  // set state factory
  repo->addComponent("state", state);
  
  // set animator factory
  IAnimatorFactory* chess_animator_factory =
    requestInterface<IAnimatorFactory>(chess->getComponent("animator_factory"));
  repo->addComponent("animator_factory", new AnimatorFactory(chess_animator_factory));
  
  // set validator
  IValidator* validator = requestInterface<IValidator>(chess->getComponent("validator"));
  repo->addComponent("validator", new Validator(validator));
  
  // set move serializers
  Repository::ComponentMap serializers = chess->listComponents("move_serializer");
  for (Repository::ComponentMap::const_iterator it = serializers.begin(),
       end = serializers.end(); it != end; ++it) {
    IMoveSerializer* s = requestInterface<IMoveSerializer>(it.value());
    if (s) repo->addComponent("move_serializer/" + it.key(), new MoveSerializer(s));
  }
  
  LOAD_TYPE(pawn);
  LOAD_TYPE(king);
  LOAD_TYPE(queen);
  LOAD_TYPE(bishop);
  LOAD_TYPE(rook);
  LOAD_TYPE(knight);
  players[0] = White::self();
  players[1] = Black::self();
  
  return repo;
}

