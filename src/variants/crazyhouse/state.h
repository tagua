/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CRAZYHOUSE__STATE_H
#define CRAZYHOUSE__STATE_H

#include <core/delegators/state.h>
#include <core/component.h>
#include <core/piece.h>

class IPoolCollection;

/**
  * @brief Namespace holding Crazyhouse components.
  */
namespace Crazyhouse {

class State : public Component, public Delegators::State {
Q_OBJECT
  IPoolCollection* m_pools;
public:
  State(IState* state);
  virtual ~State();
  
  virtual IState* clone() const;
  virtual void assign(const IState* other);
  virtual void move(const Move& m);
  
  virtual const IPoolCollection* pools() const;
  virtual IPoolCollection* pools();
};

} // namespace Crazyhouse

#endif // CRAZYHOUSE__STATE_H

