/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "animator.h"
#include <core/dropanimator.h>
#include "global.h"

namespace Crazyhouse {

AnimatorFactory::AnimatorFactory(IAnimatorFactory* parent)
: m_parent(parent) { }

IAnimator* AnimatorFactory::create(TaguaAPI* api, const INamer* namer) const {
  IAnimator* animator = m_parent->create(api, namer);
  DropAnimator* res = new DropAnimator(animator, api, namer, players);
  animator->setWarper(res);
  return res;
}

}
