/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CRAZYHOUSE__BEHAVIOUR_H
#define CRAZYHOUSE__BEHAVIOUR_H

#include <core/delegators/behaviour.h>

namespace Crazyhouse {

class Behaviour : public Delegators::Behaviour {
public:
  Behaviour(const IBehaviour* behaviour);
  
  virtual void captureOn(IState* state, const Point& square) const;
};

} // namespace Crazyhouse

#endif // CRAZYHOUSE__BEHAVIOUR_H
