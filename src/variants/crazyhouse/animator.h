/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CRAZYHOUSE__ANIMATOR_H
#define CRAZYHOUSE__ANIMATOR_H

#include <core/animator.h>
#include <core/component.h>

namespace Crazyhouse {

class AnimatorFactory : public Component, public IAnimatorFactory {
Q_OBJECT
  IAnimatorFactory* m_parent;
public:
  AnimatorFactory(IAnimatorFactory* parent);
  virtual IAnimator* create(TaguaAPI* api, const INamer* namer) const;
};

}

#endif // CRAZYHOUSE__ANIMATOR_H
