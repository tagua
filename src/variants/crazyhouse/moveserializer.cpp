/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "moveserializer.h"
#include <core/board.h>
#include <core/state.h>
#include <core/type.h>

namespace Crazyhouse {

MoveSerializer::MoveSerializer(IMoveSerializer* serializer)
: Delegators::MoveSerializer(serializer) { }

QString MoveSerializer::serialize(const Move& move, const IState* ref) const {
  if (move.drop() == Piece())
    return m_serializer->serialize(move, ref);
  
  QString res;
  if (type() == "decorated") {
    const IType* type = move.drop().type();
    if (type)
      res = "{" + move.drop().type()->name() + '}';
    else
      res = "?";
  }
  else {
    res = symbol(move.drop().type()).toUpper();
  }
  
  return res + 
         '@' + 
         move.dst().toString(ref->board()->size().y) + 
         suffix(move, ref);
}

} // namespace Crazyhouse


