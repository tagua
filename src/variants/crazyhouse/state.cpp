/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "state.h"
#include "global.h"

#include <core/behaviour.h>
#include <core/board.h>
#include <core/color.h>
#include <core/defaultpoolcollection.h>
#include <core/defaultpool.h>
#include <core/move.h>
#include <core/piece.h>
#include <core/taguacast.h>

namespace Crazyhouse {

State::State(IState* state)
: Delegators::State(state) {
  DefaultPoolCollection* c = new DefaultPoolCollection;
  for (int i = 0; i < 2; i++)
    c->addPool(players[i], new DefaultPool(players[i]));
  m_pools = c;
}

State::~State() { delete m_pools; }

IState* State::clone() const {
  State* s = new State(m_state->clone());
  s->m_pools = m_pools->clone();
  return s;
}

void State::assign(const IState* other) {
  m_state->assign(other);
  const IPoolCollection* pools = other->pools();
  if (pools) {
    delete m_pools;
    m_pools = pools->clone();
  }
}

void State::move(const Move& m) {
  if (m.drop() != Piece()) {
    Piece captured = board()->get(m.dst());
    board()->set(m.dst(), m.drop());
    pools()->pool(m.drop().color())->take(m.drop());
    
    // handle capturing by drop: some variants could use it
    if (captured != Piece()) {
      if (captured.get("promoted").toBool()) {
        captured.setType(pawn);
      }
      pools()->pool(behaviour()->opponent(captured.color()))
             ->insert(-1, captured);
    }
    
    behaviour()->advanceTurn(this);
  }
  else {
    m_state->move(m);
    if (m.promotion()) {
      Piece promoted = board()->get(m.dst());
      promoted.set("promoted", true);
      board()->set(m.dst(), promoted);
    }
  }    
}
  
const IPoolCollection* State::pools() const { return m_pools; }
IPoolCollection* State::pools() { return m_pools; }

} // namespace Crazyhouse
