/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include <KDebug>

#include <core/point.h>
#include <core/repository.h>
#include <core/variantloader.h>
#include <core/state.h>

class IBehaviour;

extern "C" KDE_EXPORT Repository* 
taguachess5x5_initrepo(IVariantLoader* loader) {
  Repository* repo = new Repository;
  Repository* chess = loader->getRepository("chess");
  if (!chess)
    // bail out if there is no chess variant
    return 0;

  repo->setProxy(chess);
  
  // set state factory
  Component* chess_state_comp = chess->getComponent("state");
  IState* chess_state = requestInterface<IState>(chess_state_comp);
  Q_ASSERT(chess_state);
  
  const IBehaviour* behaviour = chess_state->behaviour();
    
  Component* state = 0;
  bool ok = QMetaObject::invokeMethod(chess_state_comp, "clone",
    Q_RETURN_ARG(Component*, state),
    Q_ARG(const IBehaviour*, behaviour),
    Q_ARG(Component*, 0),
    Q_ARG(Point, Point(5, 5)));
  Q_ASSERT(ok);
  Q_ASSERT(state);
  
  repo->addComponent("state", state);
  
  return repo;
}

