/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CHESS__BEHAVIOUR_H
#define CHESS__BEHAVIOUR_H

#include <core/behaviour.h>
#include <core/component.h>

namespace Chess {

class Behaviour : public Component, public IBehaviour {
Q_OBJECT
public:
  virtual void captureOn(IState* state, const Point& square) const;
  virtual void move(IState* state, const Move& m) const;
  virtual void advanceTurn(IState* state) const;
  virtual Point captureSquare(const IState* state, const Move& m) const;
  virtual const IColor* opponent(const IColor* player) const;
  virtual Point direction(const IColor* player) const;
};

} // namespace Chess

#endif // CHESS__BEHAVIOUR_H

