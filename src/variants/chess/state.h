/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CHESS__STATE_H
#define CHESS__STATE_H

#include <core/board.h>
#include <core/component.h>
#include <core/state.h>
#include <core/piece.h>
#include <core/taguaobject.h>

/**
  * @brief Namespace holding Chess components.
  */
namespace Chess {

class ICastlingRules;

class State : public Component, public IState {
Q_OBJECT
  Board m_board;
  TaguaObject m_flags;
  const IColor* m_turn;
  const IBehaviour* m_behaviour;
  const ICastlingRules* m_castling_rules;
  IState* m_delegator;
protected:
  State(const State&);
public:
  State(const IBehaviour* behaviour, 
        const ICastlingRules* castling, 
        const Point& size);
public:
  virtual IState* clone() const;
  
  virtual void setup();
  
  virtual const Board* board() const;
  virtual Board* board();
  
  virtual const IColor* turn() const;
  
  virtual void setTurn(const IColor* turn);
  
  virtual bool equals(IState* other) const;
  
  virtual void assign(const IState* other);

  virtual void move(const Move& move);
  
  virtual TaguaObject* flags();
  virtual const TaguaObject* flags() const;
  
  virtual int rank(int n, const IColor* turn) const;

  virtual const IPoolCollection* pools() const;
  virtual IPoolCollection* pools();
  
  virtual const IBehaviour* behaviour() const;
  
  virtual void setDelegator(IState* delegator);
public Q_SLOTS:
  virtual Point enPassantTrigger(const Move& move) const;
  virtual Component* clone(const IBehaviour* behaviour,
                           Component* rules,
                           const Point& size) const;
};

} // namespace Chess

#endif // CHESS__STATE_H

