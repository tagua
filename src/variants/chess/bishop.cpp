/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "bishop.h"
#include <core/move.h>
#include <core/pathinfo.h>
#include <core/state.h>
#include <core/board.h>

namespace Chess {

Bishop::Bishop() { }

QString Bishop::name() const { return "bishop"; }

bool Bishop::canMove(const Piece& piece, const Piece& target,
                   Move& move, const IState* state) const {
  PathInfo path = state->board()->path(move.src(), move.dst());
  return path.diagonal() && path.clear() && target.color() != piece.color();
}

int Bishop::index() const { return 35; }

Bishop* Bishop::self() {
  static Bishop s_instance;
  return &s_instance;
}

}
