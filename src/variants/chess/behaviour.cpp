/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "behaviour.h"

#include <core/board.h>
#include <core/color.h>
#include <core/move.h>
#include <core/state.h>

namespace Chess {

void Behaviour::captureOn(IState* state, const Point& square) const {
    state->board()->set(square, Piece());
}

void Behaviour::move(IState* state, const Move& m) const {
  if (m.dst() != m.src()) {
    state->board()->set(m.dst(), state->board()->get(m.src()));
    state->board()->set(m.src(), Piece());
  }
}

void Behaviour::advanceTurn(IState* state) const {
  state->setTurn(opponent(state->turn()));
}

Point Behaviour::captureSquare(const IState*, const Move& m) const {
  if (m.type() == "en_passant_capture")
    return Point(m.dst().x, m.src().y);
  else
    return m.dst();
}

const IColor* Behaviour::opponent(const IColor* player) const {
  return player == White::self() 
    ? static_cast<IColor*>(Black::self()) 
    : static_cast<IColor*>(White::self());
}

Point Behaviour::direction(const IColor* player) const {
  return Point(0, player == White::self() ? -1 : 1);
}

} // namespace Chess


