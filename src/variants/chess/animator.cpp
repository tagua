/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "animator.h"
#include <core/animation.h>
#include <core/behaviour.h>
#include <core/board.h>
#include <core/state.h>
#include <core/move.h>
#include <core/namedsprite.h>
#include <core/piece.h>
#include <core/taguaapi.h>
#include <core/taguacast.h>
#include "knight.h"

namespace Chess {

Animator::Animator(TaguaAPI* api, const INamer* namer)
: BaseAnimator(api, namer)
, m_warper(this) { }

void Animator::setWarper(IWarper* warper) {
  m_warper = warper;
}

AnimationPtr Animator::forward(const Move& move, const IState* state) {
  AnimationPtr res = m_api->group("default");

  NamedSprite piece = m_api->takeSprite(move.src());
  Point captureSquare = move.dst();
  if (const IBehaviour* behaviour = state->behaviour())
    captureSquare = behaviour->captureSquare(state, move);
  NamedSprite captured = m_api->takeSprite(captureSquare);
  m_api->setSprite(move.dst(), piece);

  if (piece)
    res->addPreAnimation(movement(piece, move));

  if (captured)
    res->addPostAnimation(m_api->disappear(captured, "destroy"));

  if (move.promotion() && move.type() == "promotion") {
    Piece promoted = state->board()->get(move.dst());

    if (promoted != Piece()) {
      NamedSprite old_sprite = m_api->getSprite(move.dst());
      NamedSprite new_sprite = m_api->setPiece(move.dst(), promoted, false);

      res->addPostAnimation(m_api->morph(old_sprite, new_sprite, "default"));
    }
  }
  else if (move.type() == "king_castling") {
    Point rookSquare = move.dst() + Point(1,0);
    Point rookDestination = move.src() + Point(1,0);

    NamedSprite rook = m_api->takeSprite(rookSquare);
    m_api->setSprite(rookDestination, rook);
    res->addPreAnimation(m_api->move(rook, rookDestination, "default"));
  }
  else if (move.type() == "queen_castling") {
    Point rookSquare = move.dst() + Point(-2,0);
    Point rookDestination = move.src() + Point(-1,0);

    NamedSprite rook = m_api->takeSprite(rookSquare);
    m_api->setSprite(rookDestination, rook);
    res->addPreAnimation(m_api->move(rook, rookDestination, "default"));
  }

  res->addPostAnimation(m_warper->warp(state));
  return res;
}

AnimationPtr Animator::back(const Move& move, const IState* state) {
  AnimationPtr res = m_api->group("default");

  NamedSprite piece = m_api->takeSprite(move.dst());
  NamedSprite captured;
  Point captureSquare = move.dst();
  if (const IBehaviour* behaviour = state->behaviour())
    captureSquare = behaviour->captureSquare(state, move);
  Piece captured_piece = state->board()->get(captureSquare);
  if (captured_piece != Piece()) {
    captured = m_api->setPiece(captureSquare, captured_piece, false);
    res->addPreAnimation(m_api->appear(captured, "default"));
  }

  if (!piece) {
    piece = m_api->createPiece(move.dst(), state->board()->get(move.src()), false);
    res->addPreAnimation(m_api->appear(piece, "default"));
  }

  m_api->setSprite(move.src(), piece);

  if (move.promotion() && move.type() == "promotion") {
    Piece pawn_piece = state->board()->get(move.src());
    if (pawn_piece != Piece()) {
      NamedSprite pawn = m_api->createPiece(move.dst(), pawn_piece, false);
      res->addPreAnimation(m_api->morph(piece, pawn, "default"));

      // replace piece with pawn
      m_api->setSprite(move.src(), pawn);
      piece = pawn;
    }
  }
  else if (move.type() == "king_castling") {
    Point rookSquare = move.dst() + Point(1,0);
    Point rookDestination = move.src() + Point(1,0);

    NamedSprite rook = m_api->takeSprite(rookDestination);
    m_api->setSprite(rookSquare, rook);

    res->addPreAnimation(m_api->move(rook, rookSquare, "default"));
  }
  else if (move.type() == "queen_castling") {
    Point rookSquare = move.dst() + Point(-2,0);
    Point rookDestination = move.src() + Point(-1,0);

    NamedSprite rook = m_api->takeSprite(rookDestination);
    m_api->setSprite(rookSquare, rook);

    res->addPreAnimation(m_api->move(rook, rookSquare, "default"));
  }
  
  
  res->addPreAnimation(movement(piece, Move(move.dst(), move.src())));
  res->addPostAnimation(m_warper->warp(state));
  
  return res;
}

AnimationPtr Animator::movement(const NamedSprite& sprite, const Move& move) const {
  bool knight = m_api->state()->board()->get(move.src()).type() == Knight::self();
  QString flags = knight ? "lshaped rotating" : "straight";
  return m_api->move(sprite, move.dst(), flags);
}

IAnimator* AnimatorFactory::create(TaguaAPI* api, const INamer* namer) const {
  return new Animator(api, namer);
}

} // namespace Chess


