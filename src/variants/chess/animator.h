/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CHESS__ANIMATOR_H
#define CHESS__ANIMATOR_H

#include <core/baseanimator.h>

class NamedSprite;
class Piece;

namespace Chess {

class Animator : public BaseAnimator {
Q_OBJECT
  IWarper* m_warper;
protected Q_SLOTS:
  AnimationPtr movement(const NamedSprite& sprite, const Move& move) const;
public:
  Animator(TaguaAPI* api, const INamer* namer);

  void setWarper(IWarper* warper);
  virtual AnimationPtr forward(const Move& move, const IState* state);
  virtual AnimationPtr back(const Move& move, const IState* state);
};

class AnimatorFactory : public Component, public IAnimatorFactory {
Q_OBJECT
public:
  virtual IAnimator* create(TaguaAPI* api, const INamer* namer) const;
};

} // namespace Chess

#endif // CHESS__ANIMATOR_H

