/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "moveserializer.h"

#include <core/behaviour.h>
#include <core/board.h>
#include <core/move.h>
#include <core/point.h>
#include <core/state.h>
#include <core/validator.h>

#include "icsverbose.h"
#include "types.h"
#include "san.h"

namespace Chess {

MoveSerializer::MoveSerializer(const QString& rep, IValidator* validator)
: m_rep(rep)
, m_validator(validator)
, m_delegator(this) { }

QString MoveSerializer::san(const Move& move, const IState* ref) const {
  QString res;
  
  Point captureSquare = move.dst();
  if (const IBehaviour* behaviour = ref->behaviour())
    captureSquare = behaviour->captureSquare(ref, move);

  Piece piece = ref->board()->get(move.src());
  Piece captured = ref->board()->get(captureSquare);

  if (piece == Piece())
    return ""; // no piece in the initial square

  if (move.type() == "king_side_castling") {
    res = "O-O";
  }
  else if (move.type() == "queen_side_castling") {
    res = "O-O-O";
  }
  else if (piece.type() == Pawn::self()) {
    if (captured != Piece())
      res = move.src().col() + "x";

    res += move.dst().toString(ref->board()->size().y);
  }
  else {
    if (piece.type() != Pawn::self())
      res = symbol(piece.type()).toUpper();

    SAN tmp;
    tmp.src = move.src();
    tmp.dst = move.dst();
    tmp.type = piece.type();
    tmp.castling = SAN::NoCastling;
    minimal_notation(tmp, ref);

    res += tmp.src.toString(ref->board()->size().y);
    if (captured != Piece())
      res += "x";
    res += tmp.dst.toString(ref->board()->size().y);
  }

  if (move.promotion())
    res += "=" + QString(symbol(move.promotion()).toUpper());

  res += suffix(move, ref);

  return res;
}

QString MoveSerializer::serialize(const Move& move, const IState* ref) const {
  if (m_rep == "simple") {
    int ysize = ref->board()->size().y;
    QString res = move.src().toString(ysize) + move.dst().toString(ysize);
    if (move.promotion())
      res = res + "=" + 
        symbol(
          move.promotion()
        ).toUpper();
    return res;
  }
  else if (m_rep == "compact") {
    return san(move, ref);
  }
  else if (m_rep == "decorated") {
    QString res = san(move, ref);
    res.replace('K', "{king}");
    res.replace('Q', "{queen}");
    res.replace('R', "{rook}");
    res.replace('N', "{knight}");
    res.replace('B', "{bishop}");
    res.replace('P', "{pawn}");
    return res;
  }
  else {
    return "";
  }
}

QString MoveSerializer::suffix(const Move& /*move*/, const IState* /*ref*/) const {
  // FIXME use a move generator to add a suffix
  
#if 0  
  std::auto_ptr<IState> tmp(ref->clone());
  tmp->move(move);
  

  MoveGenerator generator(tmp);
  if (generator.check(Piece::oppositeColor(ref->turn()))) {
    if (generator.stalled())
      return "#";
    else
      return "+";
  }
  else {
    return "";
  }
#endif
  return "";
}

Move MoveSerializer::get_san(const SAN& san, const IState* ref) const {
  Move candidate;

  if (san.invalid())
    return candidate;

  if (san.castling != SAN::NoCastling) {
    // find king starting position
    Point from(ref->board()->size().x / 2, ref->rank(0, ref->turn()));
    Point to = from + (san.castling == SAN::KingSide? Point(2,0) : Point(-2,0));
    Piece king = ref->board()->get(from);
    if (king.type() != King::self())
      return candidate;
    else {
      candidate = Move(from, to);
      if (m_validator->legal(ref, candidate))
        return candidate;
      else
        return Move();
    }
  }

  if (san.src.valid()) {
    candidate = Move(san.src, san.dst, san.promotion);
  }
  else {
    for (int i = 0; i < ref->board()->size().x; i++) {
      for (int j = 0; j < ref->board()->size().y; j++) {
        Point p(i, j);
        Piece piece = ref->board()->get(p);
        
        Move mv(p, san.dst, san.promotion);
        if (p.resembles(san.src) && 
            piece.type() == san.type && 
            piece.color() == ref->turn()) {
          if (m_validator->legal(ref, mv)) {
            if (candidate != Move()) {
              // ambiguous!
              return Move();
            }
            else {
              // ok, we have found a candidate move
              candidate = mv;
            }
          }
        }
      }
    }
  }
  
  return candidate;
}

Move MoveSerializer::deserialize(const QString& str, const IState* ref) const {
  if (m_rep == "compact") {
    SAN tmp;
    tmp.load(str, ref->board()->size().y);
    return get_san(tmp, ref);
  }
  else if (m_rep == "ics-verbose") {
    return parse_ics_verbose(str, ref);
  }
  else {
    // no need to parse simple or decorated moves
    return Move();
  }
}


#define TRY(x) if(get_san(x, ref) != Move()) return;
void MoveSerializer::minimal_notation(SAN& san, const IState* ref) const {
  Point from = san.src;
  san.castling = SAN::NoCastling;

  // try notation without starting point
  san.src = Point::invalid();
  TRY(san);

  // add column indication
  san.src = Point(from.x, -1);
  TRY(san);

  // add row indication
  san.src = Point(-1, from.y);
  TRY(san);

  // add complete starting point
  san.src = from;
}
#undef TRY

QString MoveSerializer::symbol(const IType* type) const {
  if (!type) return "?";
  
  if (type == Knight::self())
    return "n";
  else
    return type->name().at(0);
}

Move MoveSerializer::parse_ics_verbose(const QString& str, const IState* ref) const {
  // here ref is the position _after_ this move
  ICSVerbose verbose;
  verbose.load(str, ref->board()->size().y);
  
  Point from;
  Point to;
  
  if (verbose.castling == SAN::NoCastling) {
    from = verbose.src;
    to = verbose.dst;
  }
  else {
    const IColor* opponent = ref->turn();
    if (const IBehaviour* behaviour = ref->behaviour())
      opponent = behaviour->opponent(opponent);
    from = Point(ref->board()->size().x / 2, ref->rank(0, opponent));
    to = from + (verbose.castling == SAN::KingSide ? Point(2,0) : Point(-2, 0));
  }

  return Move(from, to, verbose.promotion);
}

QString MoveSerializer::type() const { return m_rep; }

void MoveSerializer::setDelegator(IMoveSerializer* delegator) { 
  m_delegator = delegator; 
}

} // namespace Chess


