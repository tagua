/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "state.h"

#include <core/behaviour.h>
#include <core/move.h>
#include "castlingrules.h"
#include "colors.h"
#include "types.h"

namespace Chess {

State::State(const IBehaviour* behaviour, 
             const ICastlingRules* castling,
             const Point& size)
: m_board(size)
, m_behaviour(behaviour)
, m_castling_rules(castling)
, m_delegator(this) {
  m_flags.set("white_king_castling", true);
  m_flags.set("white_queen_castling", true);
  m_flags.set("black_king_castling", true);
  m_flags.set("black_queen_castling", true);
}

State::State(const State& other)
: Component()
, IState()
, m_board(other.m_board)
, m_flags(other.m_flags)
, m_turn(other.m_turn)
, m_behaviour(other.m_behaviour)
, m_castling_rules(other.m_castling_rules)
, m_delegator(this) { }

IState* State::clone() const {
  return new State(*this);
}

void State::setup() {
  for (int c = 0; c < 2; c++) {
    IColor* color = COLORS[c];
    int r0 = rank(0, color);
    int r1 = rank(1, color);
    
    for (int i = 0; i < m_board.size().x; i++) {
      m_board.set(Point(i, r1), Piece(color, Pawn::self()));
    }
    m_board.set(Point(0, r0), Piece(color, Rook::self()));
    m_board.set(Point(1, r0), Piece(color, Knight::self()));
    m_board.set(Point(2, r0), Piece(color, Bishop::self()));
    m_board.set(Point(3, r0), Piece(color, Queen::self()));
    m_board.set(Point(4, r0), Piece(color, King::self()));
    m_board.set(Point(5, r0), Piece(color, Bishop::self()));
    m_board.set(Point(6, r0), Piece(color, Knight::self()));
    m_board.set(Point(7, r0), Piece(color, Rook::self()));
  }
  
  m_turn = White::self();
}
  
const Board* State::board() const {
  return &m_board;
}

Board* State::board() {
  return &m_board;
}
  
const IColor* State::turn() const {
  return m_turn;
}
  
void State::setTurn(const IColor* turn) {
  m_turn = turn;
}
  
bool State::equals(IState* other) const {
  return m_board.equals(other->board()) &&
         m_turn == other->turn() &&
         m_flags == *other->flags();
}

void State::assign(const IState* other) {
  m_board = *other->board();
  m_turn = other->turn();
  m_flags = *other->flags();
}

void State::move(const Move& m) {
  Piece piece = m_board.get(m.src());
  if (piece == Piece()) return;

  Point captureSquare = behaviour()->captureSquare(this, m);
  behaviour()->captureOn(m_delegator, captureSquare);
  behaviour()->move(m_delegator, m);

  m_flags.set("en_passant_square", QVariant(enPassantTrigger(m)));
  const IType* promotion_type = m.promotion();

  if (promotion_type != 0) {
    m_board.set(m.dst(), Piece(piece.color(), promotion_type));
  }

  if (m_castling_rules)
    m_castling_rules->handleCastling(piece, m, m_delegator);
  behaviour()->advanceTurn(m_delegator);
}
  
const TaguaObject* State::flags() const { return &m_flags; }
TaguaObject* State::flags() { return &m_flags; }

int State::rank(int n, const IColor* turn) const {
  if (turn == White::self())
    return m_board.size().y - n - 1;
  else
    return n;
}

IPoolCollection* State::pools() { return 0; }
const IPoolCollection* State::pools() const { return 0; }

const IBehaviour* State::behaviour() const { return m_behaviour; }

Point State::enPassantTrigger(const Move& move) const {
  if (move.type() == "en_passant_trigger")
    return (move.dst() + move.src()) / 2;
  else
    return Point::invalid();
}

void State::setDelegator(IState* delegator) { m_delegator = delegator; }

Component* State::clone(const IBehaviour* behaviour, 
                        Component* castling, 
                        const Point& size) const {
  const ICastlingRules* rules = dynamic_cast<const ICastlingRules*>(castling);
  if (!rules) rules = new CastlingRulesAdaptor(castling, false);
  return new State(behaviour, rules, size);
}

} // namespace Chess
