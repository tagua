/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "castlingrules.h"

#include <KDebug>

#include <core/behaviour.h>
#include <core/board.h>
#include <core/color.h>
#include <core/move.h>
#include <core/piece.h>
#include <core/state.h>

#include "types.h"

namespace Chess {

ICastlingRules::~ICastlingRules() { }

CastlingRules::CastlingRules()
: m_delegator(this) { }

bool CastlingRules::canCastle(const Piece& piece, Move& move, const IState* state) const {
  if (move.src() != m_delegator->kingStartingPosition(state, piece.color())) return false;
  
  if (move.delta() == Point(2,0)) {
    if (state->board()->get(move.src() + Point(1,0)) == Piece() &&
        state->board()->get(move.dst()) == Piece() &&
        state->flags()->get(piece.color()->name() + "_king_castling").toBool()) {
      move.setType("king_castling");
      return true;
    }
  }
  else if (move.delta() == Point(-2,0)) {
    if (state->board()->get(move.src() - Point(1, 0)) == Piece() &&
        state->board()->get(move.dst() + Point(1, 0)) == Piece() &&
        state->board()->get(move.dst()) == Piece() &&
        state->flags()->get(piece.color()->name() + "_queen_castling").toBool()) {
      move.setType("queen_castling");
      return true;
    }
  }
  
  return false;
}

bool CastlingRules::handleCastling(const Piece& piece, const Move& move, IState* state) const {
  const IBehaviour* behaviour = state->behaviour();
  bool res = true;
  
  if (behaviour && move.type() == "king_castling") {
    Point rookSquare = move.dst() + Point(1,0);
    Point rookDestination = move.src() + Point(1,0);
    
    behaviour->move(state, Move(rookSquare, rookDestination));
  }
  else if (behaviour && move.type() == "queen_castling") {
    Point rookSquare = move.dst() - Point(2,0);
    Point rookDestination = move.src() - Point(1,0);
    
    behaviour->move(state, Move(rookSquare, rookDestination));
  }
  else { 
    res = false;
  }
   
  const IType* type = piece.type();
  TaguaObject* flags = state->flags();
  
  if (type == King::self()) {
    flags->set(piece.color()->name() + "_king_castling", false);
    flags->set(piece.color()->name() + "_queen_castling", false);
  }

  if (move.src() == Point(0,0) || move.dst() == Point(0,0))
    flags->set("black_queen_castling", false);
  if (move.src() == Point(7,0) || move.dst() == Point(7,0))
    flags->set("black_king_castling", false);
  if (move.src() == Point(0,7) || move.dst() == Point(0,7))
    flags->set("white_queen_castling", false);
  if (move.src() == Point(7,7) || move.dst() == Point(7,7))
    flags->set("white_king_castling", false);
    
  return res;
}

Point CastlingRules::kingStartingPosition(const IState* state, const IColor* player) const {
  int x = state->board()->size().x / 2;
  int y = state->rank(0, player);
  return Point(x, y);
}

void CastlingRules::setDelegator(Component* delegator) {
  m_delegator = dynamic_cast<ICastlingRules*>(delegator);
  if (!m_delegator) m_delegator = new CastlingRulesAdaptor(delegator, false);
}


CastlingRulesAdaptor::CastlingRulesAdaptor(Component* component, bool own)
: m_component(component)
, m_own(own) { }

CastlingRulesAdaptor::~CastlingRulesAdaptor() {
  if (m_own) delete m_component;
}

bool CastlingRulesAdaptor::canCastle(const Piece& piece, Move& move, const IState* state) const {
  bool result;
  QMetaObject::invokeMethod(m_component, "canCastle",
    Q_RETURN_ARG(bool, result),
    Q_ARG(Piece, piece),
    Q_ARG(Move, move),
    Q_ARG(const IState*, state));
  return result;
}

bool CastlingRulesAdaptor::handleCastling(const Piece& piece, const Move& move, IState* state) const {
  bool result;
  QMetaObject::invokeMethod(m_component, "handleCastling",
    Q_RETURN_ARG(bool, result),
    Q_ARG(Piece, piece),
    Q_ARG(Move, move),
    Q_ARG(IState*, state));
  return result;
}

void CastlingRulesAdaptor::setDelegator(Component* component) {
  QMetaObject::invokeMethod(m_component, "setDelegator", Q_ARG(Component*, component));
}

Point CastlingRulesAdaptor::kingStartingPosition(const IState* state, const IColor* player) const {
  Point res;
  QMetaObject::invokeMethod(m_component, "kingStartingPosition",
    Q_RETURN_ARG(Point, res),
    Q_ARG(const IState*, state),
    Q_ARG(const IColor*, player));
  return res;
}


} // namespace Chess


