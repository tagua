/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/


#ifndef KDE_PLUGIN_VARIANTS_H
#define KDE_PLUGIN_VARIANTS_H

#include <QStringList>
#include <KService>
#include <KServiceTypeTrader>
#include "core/variantloader.h"

class Variant;
class Repository;

class KDEPluginVariants : public IVariantLoader {
  KDEPluginVariants();
  
  Repository* load(const KService::Ptr& plugin);
  KService::Ptr get(const QString& name);
  Variant* create(const KService::Ptr& plugin);
public:
  Variant* create(const QString& name);
  virtual QStringList all() const;
  virtual Repository* getRepository(const QString& variant);
  
  /**
    * \return the singleton IVariantLoader instance
    */
  static KDEPluginVariants& self();
};

#endif // KDE_PLUGIN_VARIANTS_H
