#include <QString>
#include <core/repository.h>
#include <variant.h>
#include "builtinvariants.h"

extern "C" Repository* taguachess_initrepo(IVariantLoader*);
extern "C" Repository* taguachess5x5_initrepo(IVariantLoader*);
extern "C" Repository* taguacrazyhouse_initrepo(IVariantLoader*);

// FIXME: a KDE plugin willing to reuse a builtin one crashes

/**
  * Hardcoded definition of builtin variants.
  */
static const struct builtin {
  const char* name;
  Repository* (*init_func)(IVariantLoader*);
  const char* theme_proxy;
} builtins[] = {
  { "Chess", taguachess_initrepo, "Chess" },
  { "Chess 5x5", taguachess5x5_initrepo, "Chess" },
  { "Crazyhouse", taguacrazyhouse_initrepo, "Chess" },
};

static const struct builtin* const get_builtin(QString name) {
  for (unsigned i=0; i < sizeof(builtins)/sizeof(builtins[0]); i++)
    if (name.compare(builtins[i].name, Qt::CaseInsensitive) == 0)
      return builtins+i;
  return NULL;
}

BuiltinVariants::BuiltinVariants() {
  for (unsigned i=0; i < sizeof(builtins)/sizeof(builtins[0]); i++)
    m_allvariants << builtins[i].name;
}

BuiltinVariants& BuiltinVariants::self() {
  static BuiltinVariants inst;
  return inst;
}

Variant* BuiltinVariants::create(const QString& name) {
  Repository* repo = getRepository(name);
  if (repo) {
    const struct builtin* const builtin = get_builtin(name);
    if (!builtin)
      kFatal() << "builtin not properly registered" << name;
    return new Variant(name, repo, builtin->theme_proxy);
  }
  return NULL;
}

QStringList BuiltinVariants::all() const {
  return m_allvariants;
}

Repository* BuiltinVariants::getRepository(const QString& variant) {
  if (m_variants.contains(variant))
    return m_variants[variant];

  Repository* repo = NULL;
  const struct builtin* const builtin = get_builtin(variant);
  if (builtin) {
    repo = builtin->init_func(this);
    if (repo)
      m_variants[builtin->name] = repo;
    else
      kError() << "could not load builtin variant" << variant;
  }

  return repo;
}
