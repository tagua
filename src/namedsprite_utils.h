/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef NAMEDSPRITE_UTILS_H
#define NAMEDSPRITE_UTILS_H

#include <core/namedsprite.h>

NamedSprite duplicate(const NamedSprite& sprite);

#endif // NAMEDSPRITE_UTILS_H

