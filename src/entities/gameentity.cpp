/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "gameentity.h"

#include <core/policy.h>
#include <core/state.h>
#include <core/validator.h>

#include "chessboard.h"
#include "components.h"
#include "game.h"
#include "pgnparser.h"

using namespace boost;

GameEntity::GameEntity(Components* components, const boost::shared_ptr<Game>& game,
                       ChessBoard* chessboard, AgentGroup* group)
: UserEntity(game)
, m_components(components)
, m_chessboard(chessboard)
, m_dispatcher(group, this) {
}

QString GameEntity::save() const {
  return
    ((m_components->variant()->name() == "Chess") ? QString()
     : ("[Variant \"" + m_components->variant()->name()) + "\"]\n")
    + m_game->pgn();
}

void GameEntity::loadPGN(const PGN& pgn) {
  m_game->load(pgn);
}

StatePtr GameEntity::doMove(const Move& move) const {
  StatePtr newState(position()->clone());
  newState->move(move);
  return newState;
}

void GameEntity::executeMove(const Move& move) {
  StatePtr ref = position();
  StatePtr pos = doMove(move);
  m_game->add(move, pos);
  m_dispatcher.move(m_game->index());
}

void GameEntity::addPremove(const Move& m) {
  m_premoveQueue = m;
}

void GameEntity::cancelPremove() {
  m_premoveQueue = Move();
  m_chessboard->cancelPremove();
}

void GameEntity::notifyMove(const Index&) {
  // the other player moved: execute premove
  if (m_premoveQueue != Move() && testMove(m_premoveQueue)) {
    executeMove(m_premoveQueue);
  }
  cancelPremove();
}

void GameEntity::notifyBack() { }
void GameEntity::notifyForward() { }
void GameEntity::notifyGotoFirst() { }
void GameEntity::notifyGotoLast() { }

bool GameEntity::testMove(Move& move) const {
  return m_components->validator()->legal(position().get(), move);
}

Piece GameEntity::moveHint(const Move&) const {
//   return position()->moveHint(move);
  return Piece(); // FIXME implement move hinting
}

bool GameEntity::testPremove(const Move&) const {
  return true; // FIXME implement premove checking
}

InteractionType GameEntity::validTurn(const IColor* pool) const {
  return m_components->policy()->droppable(position().get(), m_turn_test, pool);
}

InteractionType GameEntity::validTurn(const Point& point) const {
  return m_components->policy()->movable(position().get(), m_turn_test, point);
}

bool GameEntity::movable(const Point& point) const {
  if (!m_enabled) return false;
  InteractionType action = validTurn(point);
  return m_turn_test.premove() ? action != NoAction : action == Moving;
}

bool GameEntity::oneClickMoves() const {
  return false; // FIXME support one-click moves
}

bool GameEntity::gotoFirst() {
  m_game->gotoFirst();
  return true;
}

bool GameEntity::gotoLast() {
  m_game->gotoLast();
  return true;
}

bool GameEntity::goTo(const Index& index) {
  m_game->goTo(index);
  return true;
}

bool GameEntity::forward() {
  return m_game->forward();
}

bool GameEntity::back() {
  return m_game->back();
}

bool GameEntity::undo() {
  if (m_editing_tools)
    m_game->undo();
  return true;
}

bool GameEntity::redo() {
  if (m_editing_tools)
    m_game->redo();
  return true;
}

bool GameEntity::truncate() {
  if (m_editing_tools)
    m_game->truncate();
  return true;
}
bool GameEntity::promoteVariation() {
  if (m_editing_tools)
    m_game->promoteVariation();
  return true;
}

bool GameEntity::canDetach() const {
  return true;
}


