/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>
	    (c) 2008 Yann Dirson <ydirson@altern.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "variantfactory.h"
#include <memory>
#include <KDebug>
#include "foreach.h"
#include "variant.h"
#include "variantfactories/kdepluginvariants.h"
#ifdef TAGUA_MONOLITH
# include "variantfactories/builtinvariants.h"
#endif

void load_plugins();

VariantFactory::VariantFactory() { }

VariantFactory& VariantFactory::self() {
  static VariantFactory inst;
  return inst;
}

Variant* VariantFactory::create(const QString& name) {
  Variant* v = NULL;
#ifdef TAGUA_MONOLITH
  if (v == NULL)
    v = BuiltinVariants::self().create(name);
#endif
  if (v == NULL)
    v = KDEPluginVariants::self().create(name);
  if (v == NULL)
    kFatal() << "failed to create variant" << name;
  return v;
}

QStringList VariantFactory::all() const {
  QStringList l = KDEPluginVariants::self().all();
#ifdef TAGUA_MONOLITH
  l << BuiltinVariants::self().all();
#endif
  return l;
}

Repository* VariantFactory::getRepository(const QString& variant) {
  Repository* r = NULL;
#ifdef TAGUA_MONOLITH
  if (r == NULL)
    r = BuiltinVariants::self().getRepository(variant);
#endif
  if (r == NULL)
    r = KDEPluginVariants::self().getRepository(variant);
  return r;
}
