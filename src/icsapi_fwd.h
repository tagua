/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef ICSAPI_FWD_H
#define ICSAPI_FWD_H

#include <boost/shared_ptr.hpp>

class ICSAPI;
typedef boost::shared_ptr<ICSAPI> ICSAPIPtr;

#endif // ICSAPI_FWD_H
