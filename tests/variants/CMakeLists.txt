set(main_dir "../../src/core")

SET(variants_test_SRC
  ../cppunit_main.cpp
  boardtest.cpp
  chesspiecetest.cpp
  chessgamestatetest.cpp
  chesslegalitytest.cpp
  chessserializationtest.cpp
  test_utils.cpp
)

include_directories(
  ${KDE4_INCLUDES}
  ${Boost_INCLUDE_DIRS}
  ${CMAKE_CURRENT_BINARY_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}/${main_dir}
)

add_executable(variants_test ${variants_test_SRC})
target_link_libraries(variants_test ${CPPUNIT_LIBRARIES})

add_test(variants_test variants_test)
