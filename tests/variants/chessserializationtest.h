#ifndef CHESSSERIALIZATIONTEST_H
#define CHESSSERIALIZATIONTEST_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>

class IDefaultState;
class IMoveSerializer;
class IValidator;
class Variant;

class ChessSerializationTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(ChessSerializationTest);
  CPPUNIT_TEST(test_pawn);
  CPPUNIT_TEST(test_check);
  CPPUNIT_TEST(test_check_capture);
  CPPUNIT_TEST(test_promotion);
  CPPUNIT_TEST(test_promotion_capture);
  CPPUNIT_TEST(test_promotion_check);
  CPPUNIT_TEST(test_promotion_capture_check);
  CPPUNIT_TEST(test_castling_k);
  CPPUNIT_TEST(test_castling_q);
  
  CPPUNIT_TEST(regression_knight_king);
  CPPUNIT_TEST_SUITE_END();
private:
  Variant* m_chess;
  IDefaultState* m_state;
  IValidator* m_validator;
  IMoveSerializer* m_decorator;
  IMoveSerializer* m_san;
  IMoveSerializer* m_simple;
public:
  void setUp();
  void tearDown();

  void test_pawn();
  void test_check();
  void test_check_capture();
  void test_promotion();
  void test_promotion_capture();
  void test_promotion_check();
  void test_promotion_capture_check();
  void test_castling_k();
  void test_castling_q();
  
  void regression_knight_king();
};

#endif // CHESSSERIALIZATIONTEST_H
