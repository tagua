#include "boardtest.h"

#include <board.h>
#include <piece.h>
#include <point.h>
#include <color.h>
#include <types/chess/knight.h>
#include <types/chess/king.h>

#include "test_utils.h"

using namespace Chess;

CPPUNIT_TEST_SUITE_REGISTRATION(BoardTest);

void BoardTest::setUp() {
  m_board = new Board(Point(8,8));
}

void BoardTest::tearDown() {
  delete m_board;
}

void BoardTest::test_size() {
  CPPUNIT_ASSERT_EQUAL(8, m_board->size().x);
  CPPUNIT_ASSERT_EQUAL(8, m_board->size().y);
}

void BoardTest::test_get_null() {
  Piece x = m_board->get(Point(3, 2));
  CPPUNIT_ASSERT_EQUAL(Piece(), x);
}

void BoardTest::test_set_get() {
  Piece piece(White::self(), Knight::self());
  m_board->set(Point(3, 2), piece);
  CPPUNIT_ASSERT_EQUAL(piece, m_board->get(Point(3, 2)));
}

void BoardTest::test_valid() {
  CPPUNIT_ASSERT(!m_board->valid(Point(-1, 3)));
  CPPUNIT_ASSERT(!m_board->valid(Point(7, -1)));
  CPPUNIT_ASSERT(!m_board->valid(Point(8, 0)));
  CPPUNIT_ASSERT(!m_board->valid(Point(8, 7)));
  
  CPPUNIT_ASSERT(m_board->valid(Point(1, 3)));
  CPPUNIT_ASSERT(m_board->valid(Point(4, 7)));
  CPPUNIT_ASSERT(m_board->valid(Point(0, 0)));
  CPPUNIT_ASSERT(m_board->valid(Point(7, 7)));
}

void BoardTest::test_set_invalid() {
  m_board->set(Point(8, 2), Piece(Black::self(), Knight::self()));
  CPPUNIT_ASSERT_EQUAL(Piece(), m_board->get(Point(8, 2)));
}

void BoardTest::test_compare() {
  Piece p1(White::self(), King::self());
  Piece p2(Black::self(), Knight::self());

  m_board->set(Point(3, 4), p1);
  m_board->set(Point(5, 1), p2);
  
  Board other(m_board->size());
  other.set(Point(3, 4), p1);
  
  CPPUNIT_ASSERT(!other.equals(m_board));
  
  other.set(Point(5, 1), p2);
  
  CPPUNIT_ASSERT(other.equals(m_board));
}

void BoardTest::test_clone() {
  Piece p1(White::self(), King::self());
  Piece p2(Black::self(), Knight::self());
  
  m_board->set(Point(3, 4), p1);
  
  Board other(*m_board);
  
  CPPUNIT_ASSERT_EQUAL(p1, other.get(Point(3, 4)));
  
  m_board->set(Point(3, 5), p2);
  
  CPPUNIT_ASSERT_EQUAL(Piece(), other.get(Point(3, 5)));
}

void BoardTest::test_pathinfo_h() {
  PathInfo path = m_board->path(Point(3, 4), Point(6, 4));
  CPPUNIT_ASSERT(path.direction() == PathInfo::Horizontal);
  CPPUNIT_ASSERT(path.parallel());
}

void BoardTest::test_pathinfo_v() {
  PathInfo path = m_board->path(Point(7, 2), Point(7, 4));
  CPPUNIT_ASSERT(path.direction() == PathInfo::Vertical);
  CPPUNIT_ASSERT(path.parallel());
}

void BoardTest::test_pathinfo_d() {
  PathInfo path = m_board->path(Point(3, 3), Point(5, 5));
  CPPUNIT_ASSERT(path.diagonal());

  path = m_board->path(Point(2, 7), Point(5, 4));
  CPPUNIT_ASSERT(path.diagonal());
}

void BoardTest::test_pathinfo_invalid() {
  PathInfo path = m_board->path(Point(-1, 3), Point(7, 3));
  CPPUNIT_ASSERT(!path.valid());
  
  path = m_board->path(Point(3, 3), Point(5, 4));
  CPPUNIT_ASSERT(!path.valid());
}

void BoardTest::test_pathinfo_obstacles() {
  Piece p1(White::self(), King::self());
  Piece p2(Black::self(), Knight::self());
  
  PathInfo path = m_board->path(Point(1, 1), Point(4, 4));
  CPPUNIT_ASSERT(path.clear());
  
  m_board->set(Point(2, 2), p1);
  path = m_board->path(Point(1, 1), Point(4, 4));
  CPPUNIT_ASSERT(!path.clear());
  CPPUNIT_ASSERT_EQUAL(1, path.numObstacles());
  
  m_board->set(Point(6, 2), p2);
  path = m_board->path(Point(0, 2), Point(5, 2));
  CPPUNIT_ASSERT_EQUAL(1, path.numObstacles());
  
  path = m_board->path(Point(0, 2), Point(6, 2));
  CPPUNIT_ASSERT_EQUAL(1, path.numObstacles());
  
  path = m_board->path(Point(0, 2), Point(7, 2));
  CPPUNIT_ASSERT_EQUAL(2, path.numObstacles());
  
  path = m_board->path(Point(2, 2), Point(6, 2));
  CPPUNIT_ASSERT(path.clear());
}

void BoardTest::test_find() {
  Piece p1(Black::self(), King::self());
  Piece p2(Black::self(), Knight::self());
  Piece p3(White::self(), King::self());

  m_board->set(Point(3, 4), p1);
  m_board->set(Point(5, 6), p2);
  m_board->set(Point(7, 7), p2);
  
  CPPUNIT_ASSERT(m_board->find(p3) == Point::invalid());
  CPPUNIT_ASSERT(m_board->find(p2) == Point(5, 6));
  CPPUNIT_ASSERT(m_board->find(p1) == Point(3, 4));
}





