#include "chessgamestatetest.h"
#include <board.h>
#include <color.h>
#include <defaultstate.h>
#include <move.h>
#include <piece.h>
#include <statefactory.h>
#include <types/chess/pawn.h>
#include <types/chess/king.h>
#include <types/chess/queen.h>
#include <types/chess/rook.h>
#include <types/chess/knight.h>
#include <types/chess/bishop.h>
#include <variant.h>

#include "test_utils.h"

CPPUNIT_TEST_SUITE_REGISTRATION(ChessGameStateTest);

Variant* chess_variant_factory();

using namespace Chess;

void ChessGameStateTest::setUp() {
  m_chess = chess_variant_factory();
  IStateFactory* fact = requestComponent<IStateFactory>(
    m_chess, "state_factory");
  m_state = dynamic_cast<IDefaultState*>(fact->createState());
  m_state->setup();
}

void ChessGameStateTest::tearDown() {
  delete m_state;
  delete m_chess;
}

void ChessGameStateTest::test_setup() {
  for (int i = 0; i < 8; i++) {
    CPPUNIT_ASSERT_EQUAL(
      Piece(Black::self(), Pawn::self()),
      m_state->board()->get(Point(i, 1)));
    CPPUNIT_ASSERT_EQUAL(
      Piece(White::self(), Pawn::self()),
      m_state->board()->get(Point(i, 6)));
    CPPUNIT_ASSERT_EQUAL(Piece(),
      m_state->board()->get(Point(i, 4)));
  }
  
  CPPUNIT_ASSERT_EQUAL(
    Piece(Black::self(), Rook::self()),
    m_state->board()->get(Point(0, 0)));
  CPPUNIT_ASSERT_EQUAL(
    Piece(White::self(), King::self()),
    m_state->board()->get(Point(4, 7)));
}

void ChessGameStateTest::test_simple_move() {
  m_state->move(Move(Point(4, 6), Point(4, 5))); // e3
  CPPUNIT_ASSERT_EQUAL(Piece(),
    m_state->board()->get(Point(4, 6)));
  CPPUNIT_ASSERT_EQUAL(
    Piece(White::self(), Pawn::self()),
    m_state->board()->get(Point(4, 5)));
}

void ChessGameStateTest::test_capture() {
  m_state->move(Move(Point(4, 6), Point(4, 4))); // e4
  m_state->move(Move(Point(3, 1), Point(3, 3))); // d5
  m_state->move(Move(Point(4, 4), Point(3, 3))); // exd5
  
  CPPUNIT_ASSERT_EQUAL(
    Piece(White::self(), Pawn::self()),
    m_state->board()->get(Point(3, 3)));
}

void ChessGameStateTest::test_en_passant() {
  m_state->move(Move(Point(4, 6), Point(4, 4))); // e4
  m_state->move(Move(Point(7, 1), Point(7, 2))); // h6
  m_state->move(Move(Point(4, 4), Point(4, 3))); // e5
  
  Move d5(Point(3, 1), Point(3, 3));
  d5.setType("en_passant_trigger");
  m_state->move(d5);
  
  CPPUNIT_ASSERT_EQUAL(
    Point(3, 2),
    m_state->flags().get("en_passant_square").value<Point>());
  
  Move exd6(Point(4, 3), Point(3, 2));
  exd6.setType("en_passant_capture");
  m_state->move(exd6);
  
  CPPUNIT_ASSERT_EQUAL(Piece(),
    m_state->board()->get(Point(3, 3)));
}

void ChessGameStateTest::test_kingside_castling() {
  Move oo(Point(4, 7), Point(6, 7));
  oo.setType("king_side_castling");
  m_state->move(oo);
  
  CPPUNIT_ASSERT_EQUAL(
    Piece(White::self(), King::self()),
    m_state->board()->get(Point(6, 7)));
  CPPUNIT_ASSERT_EQUAL(
    Piece(White::self(), Rook::self()),
    m_state->board()->get(Point(5, 7)));  
  CPPUNIT_ASSERT_EQUAL(Piece(),
    m_state->board()->get(Point(7, 7)));
}

void ChessGameStateTest::test_queenside_castling() {
  Move oo(Point(4, 7), Point(2, 7));
  oo.setType("queen_side_castling");
  m_state->move(oo);
  
  CPPUNIT_ASSERT_EQUAL(
    Piece(White::self(), King::self()),
    m_state->board()->get(Point(2, 7)));
  CPPUNIT_ASSERT_EQUAL(
    Piece(White::self(), Rook::self()),
    m_state->board()->get(Point(3, 7)));  
  CPPUNIT_ASSERT_EQUAL(Piece(),
    m_state->board()->get(Point(0, 7)));
}

void ChessGameStateTest::test_promotion() {
  m_state->board()->set(Point(7, 1), Piece(White::self(), Pawn::self()));
  Move h8B(Point(7, 1), Point(7, 0), Bishop::self());
  m_state->move(h8B);
  
  CPPUNIT_ASSERT_EQUAL(
    Piece(White::self(), Bishop::self()),
    m_state->board()->get(Point(7, 0)));
}


